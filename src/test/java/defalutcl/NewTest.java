package defalutcl;

import java.util.List;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class NewTest {
	WebDriver driver;

	@BeforeMethod
	@Parameters("browser")
	public void setup(String browser) {

		if (browser.equalsIgnoreCase("InternetExplorer")) {
			System.setProperty("webdriver.ie.driver", "drivers\\IEDriverServer.exe");

			driver = new InternetExplorerDriver();
		}

		else if (browser.equalsIgnoreCase("chrome")) {

			System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test
	public void testParameterWithXML() throws Exception, InterruptedException {

		String confgfile = "C:\\Users\\Tshimbiluni.Mulaudzi\\eclipse-workspace\\projectsele\\project.properties";
		FileReader fileReader = new FileReader(confgfile);
		Properties props = new Properties();
		props.load(fileReader);
		driver.get(props.getProperty("url"));

		File src = new File("data\\project.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(src);
		XSSFSheet sheet = workbook.getSheet("Sheet1");
		int rowCount = sheet.getPhysicalNumberOfRows();
		for (int i = 1; i < rowCount; i++) {

			driver.findElement(By.xpath("//*[@id=\"username\"]")).sendKeys(getCellData("username", i, sheet));
			driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys(getCellData("pass", i, sheet));

			driver.findElement(By.xpath("//*[@id=\"login\"]")).click();

			if (driver.getPageSource().contains("Hello Mulaudzi!")) {
				System.out.println("successful");
			} else if (driver.getPageSource().contains("Invalid Login details or Your Password might have expired.")) {
				System.out.println(" not successful");
			}
		}
		Select location = new Select(driver.findElement(By.xpath("//*[@id=\"location\"]")));
		location.selectByVisibleText("London");
		Select hotels = new Select(driver.findElement(By.xpath("//*[@id=\"hotels\"]")));
		hotels.selectByVisibleText("Hotel Creek");
		Select RoomType = new Select(driver.findElement(By.xpath("//*[@id=\"room_type\"]")));
		RoomType.selectByVisibleText("Double");
		Select NumberRoom = new Select(driver.findElement(By.xpath("//*[@id=\"room_nos\"]")));
		NumberRoom.selectByVisibleText("2 - Two");
		//driver.findElement(By.xpath("//*[@id=\"datepick_in\"]")).sendKeys("19022020");
		//driver.findElement(By.xpath("//*[@id=\"datepick_out\"]")).sendKeys("20022020");
		Select adultPerRoom = new Select(driver.findElement(By.xpath("//*[@id=\"adult_room\"]")));
		adultPerRoom.selectByVisibleText("2 - Two");
		Select childrenPerRoom = new Select(driver.findElement(By.xpath("//*[@id=\"child_room\"]")));
		childrenPerRoom.selectByVisibleText("1 - One");
		driver.findElement(By.xpath("//*[@id=\"Submit\"]")).click();
		WebElement radio1 = driver.findElement(By.xpath("//*[@id=\"radiobutton_0\"]"));
		radio1.click();
		driver.findElement(By.xpath("//*[@id=\"continue\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"first_name\"]")).sendKeys("Tshimbiluni");
		driver.findElement(By.xpath("//*[@id=\"last_name\"]")).sendKeys("Mashudu");
		driver.findElement(By.xpath("//*[@id=\"address\"]")).sendKeys("winne mandela zone8");
		driver.findElement(By.xpath("//*[@id=\"cc_num\"]")).sendKeys("0000222226666688");
		Select CreditType = new Select(driver.findElement(By.xpath("//*[@id=\"cc_type\"]")));
		CreditType.selectByVisibleText("Master Card");
		Select ExpireDate = new Select(driver.findElement(By.xpath("//*[@id=\"cc_exp_month\"]")));
		ExpireDate.selectByVisibleText("June");
		Select ExpireMonth = new Select(driver.findElement(By.xpath("//*[@id=\"cc_exp_year\"]")));
		ExpireMonth.selectByVisibleText("2022");
		driver.findElement(By.xpath("//*[@id=\"cc_cvv\"]")).sendKeys("020");
		driver.findElement(By.xpath("//*[@id=\"book_now\"]")).click();
		


        
        
          


            

                List<WebElement> allLinks = driver.findElements(By.xpath("//*[@id=\"order_no\"]"));

                for (  int i=0 ; i<allLinks.size();i++)
                {
               


                	    File file = new File("./read.txt");


                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(((WebElement) allLinks.get(i)).getText());
                           System.out.println(((WebElement) allLinks.get(i)).getText());
                        bw.close();
                      
	}
	}
        
	// creating method poi using loop
	public int columnCount(Sheet sheet) throws Exception {
		return sheet.getRow(0).getLastCellNum();
	}

	public String getCellData(String strColumn, int iRow, Sheet sheet) throws Exception {
		String sValue = null;
		Row row = sheet.getRow(0);
		for (int i = 0; i < columnCount(sheet); i++) {
			if (row.getCell(i).getStringCellValue().trim().equals(strColumn)) {
				Row raw = sheet.getRow(iRow);
				Cell cell = raw.getCell(i);
				DataFormatter formater = new DataFormatter();
				sValue = formater.formatCellValue(cell);
			}
		}
		return sValue;
	}
	// creating method for tests

	public ExtentReports extentReport() {
		ExtentReports extent = new ExtentReports();
		String Report;
		Report = "C:\\Users\\Tshimbiluni.Mulaudzi\\eclipse-workspace\\Selemium\\Report.html";

		ExtentHtmlReporter htmlreport = new ExtentHtmlReporter(Report);
		htmlreport.config().setTheme(Theme.DARK);
		extent.attachReporter(htmlreport);
		return extent;
	}// end of method

//creating methods of nodes
	public ExtentTest createTest(ExtentReports extent) {

		ExtentTest test = extent.createTest("MyFirstTest");
		test.pass("details");

		ExtentTest node = test.createNode("Node"); // level = 1 node.pass("details");

		return test;

	}// end method

//method for sreen shot
	public String takeScreenShot(String fileName, WebDriver driver) {
		String FileName = null;
		// String fileName = "Empty";
		try {
			File srcFile = ((TakesScreenshot) (driver)).getScreenshotAs(OutputType.FILE);

			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
			fileName = "/screenshot/" + fileName + timeStamp + ".png";

			// copying screeshot to directory
			FileUtils.copyFile(srcFile, new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fileName;

	}// end of method

}
