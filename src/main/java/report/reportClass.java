package report;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class reportClass {
	WebDriver driver;
	static ExtentReports extent;
	static ExtentTest test;

	public static void extentReport() {

		String Report;
		Report = "./reportsfile/Report.html";
		String Picture = ".r-img{width:20%}";
		
		extent = new ExtentReports();
		ExtentHtmlReporter htmlreport = new ExtentHtmlReporter(Report);
		
		htmlreport.config().setCSS(Picture);
		htmlreport.config().setTheme(Theme.DARK);
		extent.attachReporter(htmlreport);
		test = extent.createTest("invalidlog");
	
		 htmlreport.setAppendExisting(true);
		
		 
	}// end of method

	public String takeScreenShot(String string, WebDriver driver) {

		String fileName = "pic";
		try {
			File srcFile = ((TakesScreenshot) (driver)).getScreenshotAs(OutputType.FILE);

			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
			fileName = ".\\screenshot\\" + fileName + timeStamp + ".png";

			// copying screeshot to directory
			FileUtils.copyFile(srcFile, new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fileName;

	}// end of method

	public void screenfail(WebDriver driver) throws Exception {
		test = extent.createTest("Test adactin.com/HotelApp");
		String screenshot = takeScreenShot("invaild", driver);
		test.log(Status.PASS, "pass");
		test.pass("pass");
		test.pass(" invalid Sreenshot login", MediaEntityBuilder.createScreenCaptureFromPath("." + screenshot).build());

		extent.flush();
	}
	public void screen(WebDriver driver) throws Exception {
	
		String screenshot = takeScreenShot("/screenshot", driver);
		test.log(Status.PASS, "pass");
		test.pass("pass");
		test.pass("  passSreenshot", MediaEntityBuilder.createScreenCaptureFromPath("." + screenshot).build());

		extent.flush();
	}

}
