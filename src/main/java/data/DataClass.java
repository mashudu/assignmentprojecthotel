package data;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class DataClass {
	public int columnCount(Sheet sheet) throws Exception {
		return sheet.getRow(0).getLastCellNum();
	}

	public String getCellData(String strColumn, int iRow, Sheet sheet) throws Exception {
		String sValue = null;
		Row row = sheet.getRow(0);
		for (int i = 0; i < columnCount(sheet); i++) {
			if (row.getCell(i).getStringCellValue().trim().equals(strColumn)) {
				Row raw = sheet.getRow(iRow);
				Cell cell = raw.getCell(i);
				DataFormatter formater = new DataFormatter();
				sValue = formater.formatCellValue(cell);
			}
		}
		return sValue;
	}

}
