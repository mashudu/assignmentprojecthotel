package application;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.utils.Writer;


import data.DataClass;
import report.reportClass;

public class applicatio {
	Properties props = new Properties();
	WebDriver driver;
	static ExtentReports extent;
	static ExtentTest test;

	String fileName;
	DataClass obj = new DataClass();
	reportClass re = new reportClass();
	

	public void properti() throws Exception {

		String confgfile = "./project.properties";
		FileReader fileReader = new FileReader(confgfile);

		props.load(fileReader);
	}

	public void setup(String browser, String url) throws Exception {

		properti();

		if (browser.equalsIgnoreCase("chrome")) {

			System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("firefox")) {

			System.setProperty("webdriver.gecko.driver", "drivers\\geckodriver.exe");
			driver = new FirefoxDriver();

		}

		driver.get(props.getProperty("url"));
		driver.manage().timeouts().implicitlyWait(28, TimeUnit.SECONDS);

	}

	public void app() throws Exception {

		properti();

		File src = new File("data\\project.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(src);
		XSSFSheet sheet = workbook.getSheet("Sheet1");
		int rowCount = sheet.getPhysicalNumberOfRows();
		for (int i = 1; i < rowCount; i++) {

			driver.findElement(By.xpath(props.getProperty("usernam"))).sendKeys(obj.getCellData("username", i, sheet));
			driver.findElement(By.xpath(props.getProperty("pass"))).sendKeys(obj.getCellData("pass", i, sheet));

			driver.findElement(By.xpath(props.getProperty("login"))).click();
			
			 re.screen(driver);
			
		 if (driver.getPageSource().contains("Invalid Login details or Your Password might have expired.")) {
				 
				 re.screenfail(driver);
					
				}
		 else {
			 re.screen(driver);
		 }
	
		
			
		}}
			  
		
	
	

	public void seach() throws Exception {
		properti();
		

		File src = new File("data\\seachroom.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(src);
		XSSFSheet sheets = workbook.getSheet("Sheet1");
		int rowCount = sheets.getPhysicalNumberOfRows();
		for (int j = 1; j < rowCount; j++) {

			Select locations = new Select(driver.findElement(By.xpath(props.getProperty("location"))));
			locations.selectByVisibleText(obj.getCellData("location", j, sheets));
			Select hotels = new Select(driver.findElement(By.xpath(props.getProperty("hotels"))));
			hotels.selectByVisibleText(obj.getCellData("hotel", j, sheets));
			Select RoomType = new Select(driver.findElement(By.xpath(props.getProperty("room"))));
			RoomType.selectByVisibleText(obj.getCellData("roomtype", j, sheets));
			Select NumberRoom = new Select(driver.findElement(By.xpath(props.getProperty("roomno"))));
			NumberRoom.selectByVisibleText(obj.getCellData("roomNo", j, sheets));
			driver.findElement(By.xpath(props.getProperty("checkin"))).clear();
			driver.findElement(By.xpath(props.getProperty("checkin"))).sendKeys(obj.getCellData("datein", j, sheets));
			driver.findElement(By.xpath(props.getProperty("checkout"))).clear();
			driver.findElement(By.xpath(props.getProperty("checkout"))).sendKeys(obj.getCellData("dateout", j, sheets));
			Select adultPerRoom = new Select(driver.findElement(By.xpath(props.getProperty("aldult"))));
			adultPerRoom.selectByVisibleText(obj.getCellData("adult", j, sheets));
			Select childrenPerRoom = new Select(driver.findElement(By.xpath(props.getProperty("child"))));
			childrenPerRoom.selectByVisibleText(obj.getCellData("child", j, sheets));

			driver.findElement(By.xpath(props.getProperty("submit"))).click();
			re.screen(driver);
			
		}

	}

	public void selectRadio() throws Exception {

		properti();
		driver.findElement(By.xpath(props.getProperty("continue"))).click();
		re.screen(driver);
		 
		WebElement radio1 = driver.findElement(By.xpath(props.getProperty("radio")));
		radio1.click();
		driver.findElement(By.xpath(props.getProperty("continue"))).click();
		re.screen(driver);
		

	}

	public void bankDetails() throws Exception {
		properti();
		

		File src = new File("data\\bankdetails.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(src);
		XSSFSheet sheet = workbook.getSheet("Sheet1");
		int rowCount = sheet.getPhysicalNumberOfRows();
		for (int i = 1; i < rowCount; i++) {
			driver.findElement(By.xpath(props.getProperty("Fname"))).clear();
			driver.findElement(By.xpath(props.getProperty("Fname"))).sendKeys(obj.getCellData("fname", i, sheet));
			driver.findElement(By.xpath(props.getProperty("Lname"))).clear();
			driver.findElement(By.xpath(props.getProperty("Lname"))).sendKeys(obj.getCellData("lname", i, sheet));
			driver.findElement(By.xpath(props.getProperty("adress"))).clear();
			driver.findElement(By.xpath(props.getProperty("adress"))).sendKeys(obj.getCellData("adress", i, sheet));
			driver.findElement(By.xpath(props.getProperty("ccnum"))).clear();
			driver.findElement(By.xpath(props.getProperty("ccnum"))).sendKeys(obj.getCellData("ccnum", i, sheet));
			Select CreditType = new Select(driver.findElement(By.xpath(props.getProperty("cardTpye"))));
			CreditType.selectByVisibleText(obj.getCellData("cardtype", i, sheet));
			Select ExpireDate = new Select(driver.findElement(By.xpath(props.getProperty("month"))));
			ExpireDate.selectByVisibleText(obj.getCellData("month", i, sheet));
			Select ExpireMonth = new Select(driver.findElement(By.xpath(props.getProperty("year"))));
			ExpireMonth.selectByVisibleText(obj.getCellData("year", i, sheet));
			driver.findElement(By.xpath(props.getProperty("ccvv"))).sendKeys(obj.getCellData("ccvv", i, sheet));

			driver.findElement(By.xpath(props.getProperty("book"))).click();
			re.screen(driver);
			
		}

	}

	public void orderno() throws Exception {
		properti();
		String ordernu = driver.findElement(By.xpath(props.getProperty("order"))).getAttribute("value");
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("DD-MM-yyyy HH-mm" + "\n");

		File file = new File("./read.txt");

		Writer writer = null;

		try (FileWriter wr = new FileWriter(file, true); BufferedWriter bw = new BufferedWriter(wr)) {
			bw.write("order" + ordernu + date + dateFormat + "\n");

		} catch (IOException ex) {
			System.out.println("empty");
		}
		properti();
		driver.findElement(By.xpath(props.getProperty("iteration"))).click();
		driver.findElement(By.xpath(props.getProperty("seachHotel"))).click();
		re.screen(driver);
		driver.findElement(By.xpath(props.getProperty("seachor"))).sendKeys(ordernu);
		driver.findElement(By.xpath(props.getProperty("seachHotel"))).click();
		re.screen(driver);
		driver.findElement(By.xpath(props.getProperty("logout"))).click();

		re.screen(driver);
		 
		driver.quit();
	}

}
